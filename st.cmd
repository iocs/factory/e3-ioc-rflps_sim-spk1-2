require essioc
require iocmetadata
require rflps_sim,1.1.4+0

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX", "Spk-010RFC:")
epicsEnvSet("SECTION", "Spk")
epicsEnvSet("KLY", "2")
epicsEnvSet("PLCIP", "rflps-sim-spk-010-plc-02.tn.esss.lu.se")

## PLC Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "352")
epicsEnvSet("OUTSIZEAF", "208")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "192")
epicsEnvSet("OUTSIZEDIO", "32")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "390")
epicsEnvSet("OUTSIZEPSU", "222")


s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

dbLoadTemplate("$(rflps_sim_DB)/$(SECTION)_rflpsCPU.substitutions", "PREFIX=$(PREFIX), KLY=$(KLY)")
dbLoadTemplate("$(rflps_sim_DB)/$(SECTION)_rflpsAF.substitutions", "PREFIX=$(PREFIX), KLY=$(KLY)")
dbLoadTemplate("$(rflps_sim_DB)/$(SECTION)_rflpsDIO.substitutions", "PREFIX=$(PREFIX), KLY=$(KLY)")
dbLoadTemplate("$(rflps_sim_DB)/$(SECTION)_rflpsPSU.substitutions", "PREFIX=$(PREFIX), KLY=$(KLY)")

pvlistFromInfo ARCHIVE_THIS "$(IOCNAME):ArchiverList"
pvlistFromInfo SAVRES_THIS "$(IOCNAME):SavResList"

iocInit()

